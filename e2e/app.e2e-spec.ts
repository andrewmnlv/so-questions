import { SoQuestionsPage } from './app.po';

describe('so-questions App', () => {
  let page: SoQuestionsPage;

  beforeEach(() => {
    page = new SoQuestionsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('soq works!');
  });
});
