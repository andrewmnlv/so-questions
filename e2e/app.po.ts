import { browser, element, by } from 'protractor';

export class SoQuestionsPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('soq-root h1')).getText();
  }
}
