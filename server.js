var express = require("express");
var compression = require('compression');

var app = express();
var port = process.env.PORT || 8080;
var distDir = __dirname + "/dist/";

app.use(compression());
app.use(express.static(distDir));

app.listen(port, function () {
  console.log("App now running on port", port);
});
