import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'soq-infinite-scroll',
  templateUrl: './infinite-scroll.component.html',
  styleUrls: ['./infinite-scroll.component.scss']
})
export class InfiniteScrollComponent {

  @Input() page = 1;
  @Input() disable = false;
  @Input() allowLoad = true;

  @Output() loadNextPage = new EventEmitter<{ page: number }>();

  constructor() { }

  onScrollDown() {
    if (this.allowLoad) {
      this.loadNextPage.emit({ page: this.page + 1 });
    }
  }
}
