import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';

import { ErrorAlertComponent } from './error-alert/error-alert.component';
import { LoadingIndicatorComponent } from './loading-indicator/loading-indicator.component';
import { InfiniteScrollComponent } from './infinite-scroll/infinite-scroll.component';

@NgModule({
  imports: [
    CommonModule,
    InfiniteScrollModule,
  ],
  declarations: [
    ErrorAlertComponent,
    LoadingIndicatorComponent,
    InfiniteScrollComponent,
  ],
  exports: [
    ErrorAlertComponent,
    LoadingIndicatorComponent,
    InfiniteScrollComponent,
  ]
})
export class SharedModule {
}
