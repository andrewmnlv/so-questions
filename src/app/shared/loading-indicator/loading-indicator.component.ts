import { Component, Input } from '@angular/core';

@Component({
  selector: 'soq-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.scss']
})
export class LoadingIndicatorComponent {

  @Input() loading: boolean;

  constructor() { }

}
