export interface Owner {
  display_name: string;
  profile_image: string;
}
