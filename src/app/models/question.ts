import { Owner } from './owner';

export interface Question {
  question_id: number;
  title: string;
  creation_date: string;
  owner: Owner;
  body: string;
}
