import { Action } from '@ngrx/store';
import { Question } from '../models/question';
import { type } from '../util';

export const ActionTypes = {
  LOAD: type('[Question] Load'),
  LOAD_SUCCESS: type('[Question] Load Success'),
  LOAD_FAIL: type('[Question] Load Fail'),
  SELECT: type('[Question] Select'),
  DESELECT: type('[Question] Deselect'),
};

export class LoadAction implements Action {
  type = ActionTypes.LOAD;

  constructor(public payload: { page?: number } = {}) { }
}

export class LoadSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;

  constructor(public payload: { has_more: boolean, items: Question[], page: number }) { }
}

export class LoadFailAction implements Action {
  type = ActionTypes.LOAD_FAIL;

  constructor(public payload: any) { }
}

export class SelectAction implements Action {
  type = ActionTypes.SELECT;

  constructor(public payload: number) { }
}

export class DeselectAction implements Action {
  type = ActionTypes.DESELECT;

  constructor(public payload?: any) { }
}

export type Actions = LoadAction | LoadSuccessAction | LoadFailAction | SelectAction | DeselectAction;
