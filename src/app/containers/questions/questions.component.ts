import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Question } from '../../models/question';

import * as fromRoot from '../../reducers';
import * as question from '../../actions/question';

@Component({
  selector: 'soq-questions',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent {

  questions$: Observable<Question[]>;
  questionDetail$: Observable<Question>;
  loading$: Observable<boolean>;
  hasMore$: Observable<boolean>;
  page$: Observable<number>;
  error$: Observable<any>;

  constructor(private store: Store<fromRoot.State>) {
    this.questions$ = store.select(fromRoot.getQuestionsList);
    this.questionDetail$ = store.select(fromRoot.getSelectedQuestion);
    this.loading$ = store.select(fromRoot.getQuestionsLoading);
    this.hasMore$ = store.select(fromRoot.getQuestionsHasMore);
    this.page$ = store.select(fromRoot.getQuestionsPage);
    this.error$ = store.select(fromRoot.getQuestionsError);
    this.store.dispatch(new question.LoadAction());
  }

  loadNextPage(options?: any) {
    this.store.dispatch(new question.LoadAction(options));
  }

  showModal(questionId: number) {
    this.store.dispatch(new question.SelectAction(questionId));
  }

  deselect() {
    this.store.dispatch(new question.DeselectAction());
  }

}
