import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { SharedModule } from './shared/shared.module';
import { ComponentsModule } from './components/components.module';
import { QuestionEffects } from './effects/question';

import { AppComponent } from './app.component';
import { QuestionsComponent } from './containers/questions/questions.component';

import { SOApiService } from './services/so-api.service';

import { reducer } from './reducers';

@NgModule({
  declarations: [
    AppComponent,
    QuestionsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CommonModule,
    ComponentsModule,
    SharedModule,
    // Store setup
    StoreModule.provideStore(reducer),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
    EffectsModule.run(QuestionEffects),
  ],
  providers: [SOApiService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
