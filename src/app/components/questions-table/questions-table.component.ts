import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Question } from '../../models/question';

@Component({
  selector: 'soq-questions-table',
  templateUrl: './questions-table.component.html',
  styleUrls: ['./questions-table.component.scss']
})
export class QuestionsTableComponent {

  @Input() questions: Question[];

  @Output() showModal = new EventEmitter<number>();

  constructor() { }

  onClick(e, questionId: number) {
    e.preventDefault();
    this.showModal.emit(questionId);
  }

}
