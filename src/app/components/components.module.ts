import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ng2-bootstrap/modal';

import { QuestionsTableComponent } from './questions-table/questions-table.component';
import { QuestionDetailModalComponent } from './question-detail-modal/question-detail-modal.component';

export const COMPONENTS = [
  QuestionsTableComponent,
  QuestionDetailModalComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot(),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ComponentsModule {
}
