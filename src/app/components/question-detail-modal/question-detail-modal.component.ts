import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalDirective } from 'ng2-bootstrap/modal';

import { Question } from '../../models/question';

@Component({
  selector: 'soq-question-detail-modal',
  templateUrl: './question-detail-modal.component.html',
  styleUrls: ['./question-detail-modal.component.scss']
})
export class QuestionDetailModalComponent implements OnInit {

  @Input() question: Question;

  @Output() onHide = new EventEmitter();

  @ViewChild('modal') modal: ModalDirective;

  constructor(private domSanitizer: DomSanitizer) { }

  ngOnInit() { }

  hideModal(): void {
    this.modal.hide();
  }

  onHidden(): void {
    this.onHide.emit();
  }

  get body() {
    return this.domSanitizer.bypassSecurityTrustHtml(this.question.body);
  }

}
