import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SOApiService {

  private API_PATH = 'https://api.stackexchange.com/2.2/questions';

  constructor(private http: Http) { }

  getQuestions(options: any = {}): Observable<any> {
    const search = new URLSearchParams();
    search.set('site', 'stackoverflow');
    search.set('filter', '!b0OfNHmfSZf9Ua');

    if (options.page) {
      search.set('page', options.page);
    }

    return this.http.get(this.API_PATH, {
        search: search
      })
      .map(this.extractData)
      .catch(this.handleError);
  }


  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error_message || JSON.stringify(body);
      errMsg = `${error.status} ${error.statusText || ''} - ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
