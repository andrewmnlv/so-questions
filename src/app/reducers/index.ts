import { createSelector } from 'reselect';
import { ActionReducer } from '@ngrx/store';
import { compose } from '@ngrx/core/compose';
import { storeFreeze } from 'ngrx-store-freeze';
import { combineReducers } from '@ngrx/store';

import { environment } from '../../environments/environment';

import * as fromQuestions from './questions';

export interface State {
  questions: fromQuestions.State;
}

const reducers = {
  questions: fromQuestions.reducer,
};

const developmentReducer: ActionReducer<State> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<State> = combineReducers(reducers);

export function reducer(state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}

export const getQuestionsState = (state: State) => state.questions;

export const getQuestionsLoading = createSelector(getQuestionsState, fromQuestions.getLoading);
export const getQuestionsHasMore = createSelector(getQuestionsState, fromQuestions.getHasMore);
export const getQuestionsPage = createSelector(getQuestionsState, fromQuestions.getPage);
export const getQuestionsError = createSelector(getQuestionsState, fromQuestions.getError);
export const getQuestionsList = createSelector(getQuestionsState, fromQuestions.getAll);
export const getSelectedQuestion = createSelector(getQuestionsState, fromQuestions.getSelected);
