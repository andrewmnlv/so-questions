import { createSelector } from 'reselect';
import { Question } from '../models/question';
import * as question from '../actions/question';

export interface State {
  ids: number[];
  entities: { [id: number]: Question };
  selectedQuestionId: number | null;
  page: number;
  has_more: boolean;
  loading: boolean;
  error: any;
}

export const initialState: State = {
  ids: [],
  entities: {},
  selectedQuestionId: null,
  page: 1,
  has_more: true,
  loading: false,
  error: null
};

export function reducer(state = initialState, action: question.Actions): State {
  switch (action.type) {
    case question.ActionTypes.LOAD:
      return Object.assign({}, state, {
        loading: true,
      });

    case question.ActionTypes.LOAD_SUCCESS:
      const data = action.payload;
      const questions = action.payload.items;
      const newQuestions = questions.filter(question => !state.entities[question.question_id]);

      const newQuestionsIds = newQuestions.map(question => question.question_id);
      const newQuestionsEntities = newQuestions.reduce((entities: { [question_id: string]: Question }, question: Question) => {
        return Object.assign(entities, {
          [question.question_id]: question
        });
      }, {});

      return Object.assign({}, state, {
        ids: [...state.ids, ...newQuestionsIds],
        entities: Object.assign({}, state.entities, newQuestionsEntities),
        has_more: data.has_more,
        loading: false,
        page: action.payload.page,
      });

    case question.ActionTypes.LOAD_FAIL:
      return Object.assign({}, state, {
        error: action.payload,
        loading: false,
      });

    case question.ActionTypes.SELECT:
      return Object.assign({}, state, {
        selectedQuestionId: action.payload,
      });

    case question.ActionTypes.DESELECT:
      return Object.assign({}, state, {
        selectedQuestionId: null,
      });

    default:
      return state;
  }
}

export const getEntities = (state: State) => state.entities;

export const getIds = (state: State) => state.ids;

export const getSelectedId = (state: State) => state.selectedQuestionId;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;

export const getHasMore = (state: State) => state.has_more;

export const getPage = (state: State) => state.page;

export const getSelected = createSelector(getEntities, getSelectedId, (entities, selectedId) => {
  return entities[selectedId] || null;
});

export const getAll = createSelector(getEntities, getIds, (entities, ids) => {
  return ids.map(id => entities[id]);
});
