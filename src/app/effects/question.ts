import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { SOApiService } from '../services/so-api.service';
import * as question from '../actions/question';

@Injectable()
export class QuestionEffects {

  @Effect()
  load$: Observable<Action> = this.actions$
    .ofType(question.ActionTypes.LOAD)
    .map(toPayload)
    .switchMap((payload) => {
      return this.soApiService.getQuestions(payload)
        .map(data => new question.LoadSuccessAction(data))
        .catch(error => of(new question.LoadFailAction(error)));
    });

  constructor(private actions$: Actions, private soApiService: SOApiService) { }
}
